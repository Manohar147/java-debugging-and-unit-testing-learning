
import static org.junit.Assert.*;

public class UtilitiesTest {

    private Utilities utilities;

    @org.junit.Before
    public void before() {
        utilities = new Utilities();
    }

    @org.junit.Test
    public void everyNthChar() {

        assertArrayEquals(new char[]{'e', 'l'}, utilities.everyNthChar(new char[]{'h', 'e', 'l', 'l', 'o'}, 2));
        assertArrayEquals(new char[]{'h', 'e', 'l', 'l', 'o'}, utilities.everyNthChar(new char[]{'h', 'e', 'l', 'l', 'o'}, 7));
    }

    @org.junit.Test
    public void removePairs() {
        assertEquals("ABCDE", utilities.removePairs("ABBCDEE"));
        assertEquals("ABCDE", utilities.removePairs("ABCCDDEE"));
        assertNull("Null was passed", utilities.removePairs(null));
    }

    @org.junit.Test
    public void converter() {
        assertEquals(300, utilities.converter(10, 5));
    }

    @org.junit.Test(expected = ArithmeticException.class)
    public void converter_with_zero() {
        utilities.converter(10, 0);
    }

    @org.junit.Test
    public void nullIfOddLength() {
        assertNull("Did not get null for odd length", utilities.nullIfOddLength("aabbccd"));
        assertNull("Did not get null for null", utilities.nullIfOddLength(null));
        assertNotNull("Did  get null for even length", utilities.nullIfOddLength("aabbcc"));
    }
}