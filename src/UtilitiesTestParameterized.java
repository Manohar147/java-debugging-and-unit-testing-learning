import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class UtilitiesTestParameterized {

    private Utilities utilities;

    private String input;
    private String output;

    public UtilitiesTestParameterized(String input, String output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getParams() {
        return Arrays.asList(new Object[][]{
                {"ABCDEFF", "ABCDEF"},
                {"ABCDEFFG", "ABCDEFG"},
                {"A", "A"}
        });
    }

    @org.junit.Before
    public void before() {
        utilities = new Utilities();
    }

    @org.junit.Test
    public void removePairs() {
        assertEquals(output, utilities.removePairs(input));
    }


}
